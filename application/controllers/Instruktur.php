<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instruktur extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		// var_dump($this->session->userdata('is_login')); die;
		if (!$this->session->userdata('is_login'))
		{
			return redirect('login');
		}
	}

	public function kelas()
	{
		$data 		= $this->model_kelas->getKelasByInstruktur();
		$instruktur = $this->model_instruktur->getSession();

		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('instruktur/kelas', compact('data', 'instruktur'));
		$this->load->view('layouts/footer');
	}

	public function insertKelas()
	{
		if (isset($_FILES['video']['name']) && $_FILES['video']['name'] != '' && isset($_FILES['thumbnail']['name']) && $_FILES['thumbnail']['name'] != '') {
			$date = date("ymd");
			unset($config);

			$config['upload_path'] = './assets/file-kelas';
			$config['max_size'] = '60000';
			$config['allowed_types'] = 'avi|flv|wmv|mp3|mp4|gif|jpg|png|jpeg';
			$config['overwrite'] = FALSE;
			$config['remove_spaces'] = TRUE;
			$video_name = $_FILES['video']['name'];
			$video_name = str_replace(' ', '_', $video_name);
			$thumbnail_name = $_FILES['thumbnail']['name'];
			$thumbnail_name = str_replace(' ', '_', $thumbnail_name);
			$config['file_name_video'] = $video_name;
			$config['file_name_thumbnail'] = $thumbnail_name;

			$this->load->library('upload', $config);

			for ($i=0; $i<2; $i++)
			{
				if ($i == 0)
				{
					if(!$this->upload->do_upload('video')) {
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('danger', "Data Kelas Gagal Ditambahkan, Kegagalan Sistem Saat Upload Video $error");
						return redirect('instruktur/kelas');
					}
				} else {
					if(!$this->upload->do_upload('thumbnail')) {
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('danger', "Data Kelas Gagal Ditambahkan, Kegagalan Sistem Saat Upload Thumbnail $error");
						return redirect('instruktur/kelas');
					}
				}
			}
			
				$videoDetails = $this->upload->data();
				$data['video_name']= $config['file_name_video'];
				$data['video_detail'] = $videoDetails;
				
				$thumbnailDetails = $this->upload->data();
				$data['thumbnail_name']= $config['file_name_thumbnail'];
				$data['thumbnail_detail'] = $thumbnailDetails;

				$dataInsert = [
					'nama_kelas'		=> $this->input->post('nama_kelas'),	
					'deskripsi_kelas'	=> $this->input->post('deskripsi_kelas'),
					'id_kategori'		=> $this->input->post('id_kategori'),
					'id_instruktur'		=> $this->session->userdata('id_user'),
					'tag1'				=> $this->input->post('tag1'),
					'tag2'				=> $this->input->post('tag2'),
					'tag3'				=> $this->input->post('tag3'),
					'tag4'				=> $this->input->post('tag4'),
					'tag5'				=> $this->input->post('tag5'),
					'thumbnail'			=> $data['thumbnail_name'],
					'file_video'		=> $data['video_name'],
					
				]; 

				$insert = $this->model_kelas->insert($dataInsert);

				if ($insert)
				{
					$this->session->set_flashdata('success', 'Data Kelas Ditambahkan');
				} else 
				{
					$this->session->set_flashdata('danger', 'Data Kelas Gagal Ditambahkan');
				}
				return redirect('instruktur/kelas');
	
		}else{
			$this->session->set_flashdata('danger', 'Data Kelas Gagal Ditambahkan, Tolong Pilih Video Materi');
			return redirect('instruktur/kelas');
		}
	  }

	public function editKelas($id)
	{
		$instruktur = $this->model_instruktur->get();
		$data		= $this->model_kelas->show('id', $id)->row();

		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('instruktur/edit_kelas', compact('data', 'instruktur'));
		$this->load->view('layouts/footer');
	}

	public function updateKelas()
	{
		if (isset($_FILES['video']['name']) && $_FILES['video']['name'] != '') {
			unset($config);
			$date = date("ymd");
			$configVideo['upload_path'] = './assets/video-kelas';
			$configVideo['max_size'] = '60000';
			$configVideo['allowed_types'] = 'avi|flv|wmv|mp3|mp4';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$video_name = $date.$_FILES['video']['name'];
			$configVideo['file_name'] = $video_name;

			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if(!$this->upload->do_upload('video')) {
				$dataUpdate = [
					'nama_kelas'		=> $this->input->post('nama_kelas'),	
					'deskripsi_kelas'	=> $this->input->post('deskripsi_kelas'),
					'id_instruktur'		=> $this->input->post('id_instruktur'),
					'tag1'				=> $this->input->post('tag1'),
					'tag2'				=> $this->input->post('tag2'),
					'tag3'				=> $this->input->post('tag3'),
					'tag4'				=> $this->input->post('tag4'),
					'tag5'				=> $this->input->post('tag5'),
				]; 
			}else{
				$videoDetails = $this->upload->data();
				$data['video_name']= $configVideo['file_name'];
				$data['video_detail'] = $videoDetails;
				$dataUpdate = [
					'nama_kelas'		=> $this->input->post('nama_kelas'),	
					'deskripsi_kelas'	=> $this->input->post('deskripsi_kelas'),
					'id_instruktur'		=> $this->input->post('id_instruktur'),
					'tag1'				=> $this->input->post('tag1'),
					'tag2'				=> $this->input->post('tag2'),
					'tag3'				=> $this->input->post('tag3'),
					'tag4'				=> $this->input->post('tag4'),
					'tag5'				=> $this->input->post('tag5'),
					'file_video'		=> $data['video_name'],
				]; 
				
			}

		}else{
			$dataUpdate = [
				'nama_kelas'		=> $this->input->post('nama_kelas'),	
				'deskripsi_kelas'	=> $this->input->post('deskripsi_kelas'),
				'id_instruktur'		=> $this->input->post('id_instruktur'),
				'tag1'				=> $this->input->post('tag1'),
				'tag2'				=> $this->input->post('tag2'),
				'tag3'				=> $this->input->post('tag3'),
				'tag4'				=> $this->input->post('tag4'),
				'tag5'				=> $this->input->post('tag5'),
			]; 
		}
		
		$update = $this->model_kelas->update($this->input->post('id'), $dataUpdate);

		if ($update)
		{
			$this->session->set_flashdata('success', 'Data Kelas Diperbarui');
		} else 
		{
			$this->session->set_flashdata('error', 'Data Kelas Gagal Diperbarui');
		}

		return redirect('instruktur/kelas');
	}

	public function deleteKelas($id)
	{
		$delete	= $this->model_kelas->delete($id);
		if ($delete)
		{
			$this->session->set_flashdata('success', 'Data Kelas Dihapus');
		} else 
		{
			$this->session->set_flashdata('success', 'Data Kelas Gagal Diperbarui');

		}
		return redirect('instruktur/kelas');
	}

	public function pesertaKelas($idKelas)
	{
		$kelas 		= $this->model_kelas->show('id', $idKelas)->row();
		$peserta 	= $this->model_peserta->showByIdKelas('id_kelas', $idKelas)->result_array();

		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('instruktur/peserta', compact('kelas', 'peserta'));
		$this->load->view('layouts/footer');
	}

	
}
