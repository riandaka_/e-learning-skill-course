<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}

	public function aksi()
	{
		$data	= [
			'email'			=> $this->input->post('email'),
			'password'		=> md5($this->input->post('pass')),
			'nama_lengkap'	=> $this->input->post('nama_lengkap'),
			'tanggal_lahir'	=> $this->input->post('tanggal_lahir'),
		];

		$insert	= $this->model_user->insert($data);

		if ($insert)
		{
			echo "<script>
			alert('Terimaksih Telah Mendaftar Menjadi Peserta!');
			window.location.href='welcome';
			</script>";
		} else {
			echo "<script>
			alert('Ada Keasalahan Teknis Saat Mendaftar!');
			window.location.href='welcome';
			</script>";

		}
	}

	public function daftarKelas()
	{
		$data	= [
			'id_user'	=> $this->session->userdata('id_user'),
			'id_kelas'	=> $this->input->post('id_kelas')
		];

		$daftar	= $this->model_peserta->daftarKelas($data);
		
		if ($data) {
			echo "<script>
			alert('Terimakasih telah mendaftar kelas!');
			window.location.href='welcome';
			</script>";
		} else {
			echo "<script>
			alert('Ada Keasalahan Teknis Saat Mendaftar Kelas!');
			window.location.href='welcome';
			</script>";
		}
	}

}
