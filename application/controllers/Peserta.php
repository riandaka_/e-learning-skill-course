<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('is_login'))
		{
			return redirect('peserta');
		}
	}
	public function index()
	{
		var_dump('login_sukses'); die;
	}

	public function kelas()
	{
		$checkAssign	= $this->model_rating->checkAssign();
		if ($checkAssign == 0) {
			$this->load->view('front/kelas_saya_empty');
		} else 
		{

		$idPeserta	= $this->session->userdata('id_user');
		$data		= $this->model_peserta->getByPeserta($idPeserta);

		/* MULAI IBCF */

		// SIMILIARTITY
		$kelas		= $this->model_kelas->get();
		$pasangan	= [];
		$response	= [];
		foreach($kelas as $k)
		{
			foreach($kelas as $ke)
			{
				$cekPasangan 		= $ke['id_kelas'].','.$k['id_kelas'];
				if (!in_array($cekPasangan, $pasangan)) {

					if ($k['id_kelas'] != $ke['id_kelas']) 
					{
						$rating1	= $this->model_rating->show('id_kelas', $k['id_kelas']);
						if ($rating1->num_rows() != 0)
						{
							$rating1 = $rating1->result_array();
							$sigmaBawahKiri 	= 0;
							$sigmaBawahKanan 	= 0;
							foreach($rating1 as $rating1)
							{
								$rating2	= $this->model_rating->show2('id_kelas', $ke['id_kelas'], 'id_user', $rating1['id_user']);
								if ($rating2->num_rows() != 0)
								{
									$nilaiAtas 			= 0;
	
									$mean				= $this->model_rating->mean($rating1['id_user']);
	
									$atasKiri			= $rating1['rating'] - $mean->mean;
									$atasKanan			= $rating2->row()->rating - $mean->mean;
									$finalAtas			= $atasKiri * $atasKanan;
									
									$kuadratBawahKiri	= pow($atasKiri, 2);
									$kuadratBawahKanan	= pow($atasKanan, 2);
									$nilaiAtas 			+= $finalAtas;
									$sigmaBawahKiri 	+= $kuadratBawahKiri;
									$sigmaBawahKanan 	+= $kuadratBawahKanan;
									
								}
							}
	
							$akarBawahKiri	=	sqrt($sigmaBawahKiri);
							$akarBawahKanan	=	sqrt($sigmaBawahKanan);
							$finalBawah		=	$akarBawahKiri * $akarBawahKanan;
							if ($nilaiAtas == 0 || $finalBawah == 0)
							{
								$similarity = 0.98;
							} else {
								$similarity		=	$nilaiAtas / $finalBawah;
							}
							$respFinal 	= [
								'kelasA'		=> $k['id_kelas'],
								'kelasB'		=> $ke['id_kelas'],
								'similarity'	=> round($similarity, 9)
							];
	
							$respPasangan	=	$k['id_kelas'].",".$ke['id_kelas'];
							array_push($pasangan, $respPasangan);
							array_push($response, $respFinal);
						}
					}
				}
			}
		}

		//PREDIKSI RATING
		$jumlahRating	= $this->model_rating->sum();
		
		$arrayKelas		= $this->model_kelas->getID();

		$dataKelasUser 	= $this->model_rating->showID('id_user', $idPeserta);
		$kelasUser		= [];

		$asyncKelas		= [];

		$finalPrediksi	= [];

		foreach ($dataKelasUser as $dataKelasU)
		{
			array_push($kelasUser, $dataKelasU['id_kelas']);
		}

		foreach ($arrayKelas as $ak)
		{
			if (!in_array($ak['id'], $kelasUser)) {
				array_push($asyncKelas, $ak);
			}
		}

		$finalResponse 	= [];
		$maeAtas		= 0;
			foreach ($asyncKelas as $ak)
			{
				$prediksiRatingAtas		= 0;
				$prediksiRatingBawah	= 0;

				foreach ($kelasUser as $ku)
				{
						$showRating				= $this->model_rating->show2('id_user', $idPeserta, 'id_kelas', $ku)->row();

						foreach ($response as $resp)
						{
							if ( ($ku == $resp['kelasA'] && $ak['id'] == $resp['kelasB']) || ($ku == $resp['kelasB'] && $ak['id'] == $resp['kelasA']))
							{
								$operasiAtasPrediksi	= $showRating->rating * $resp['similarity'];
								$prediksiRatingAtas 	+= $operasiAtasPrediksi;
								
								$prediksiRatingBawah	+= abs($resp['similarity']);
							}
								
						}
				}
				$finalResult			= $prediksiRatingAtas / $prediksiRatingBawah;
				$supplyFinalResponse 	= [
					'atas'				=> $prediksiRatingAtas,
					'bawah'				=> $prediksiRatingBawah,
					'id_kelas_async'	=> $ak['id'],
					'prediksi_rating'	=> $finalResult
				];
				array_push($finalResponse, $supplyFinalResponse);
				$maeAtas 				+= abs($finalResult);
			}
			
		usort($finalResponse, function($a, $b) {
            return $a['prediksi_rating'] <= $b['prediksi_rating'];
        });
		

		$displayRekomendasi	= [];
		
		for ($i=0; $i<5; $i++)
		{
			$dataRekomen	= $this->model_kelas->show('id', $finalResponse[$i]['id_kelas_async'])->row();
			$forRekomen		= [
				'id_kelas'			=> $dataRekomen->id,
				'nama_kelas'		=> $dataRekomen->nama_kelas,
				'deskripsi_kelas'	=> $dataRekomen->deskripsi_kelas,
				'thumbnail'			=> $dataRekomen->thumbnail,
			];
			array_push($displayRekomendasi, $forRekomen);
		}

		$mae				= $maeAtas / $jumlahRating;
		$updateMae			= $this->model_mae->update($idPeserta, $mae);

		$this->load->view('front/kelas_saya', compact('data', 'displayRekomendasi', 'mae'));
		}
	}

	public function tontonKelas()
	{
		$idUser		= $_GET['iu'];
		$idKelas	= $_GET['ik'];

		$check		= $this->model_rating->check($idUser, $idKelas);
		$kelas 		= $this->model_kelas->show('id', $idKelas)->row();

		$this->load->view('front/tonton_kelas', compact('check', 'kelas'));
	}

	public function submitRating()
	{
		$prev	= $this->agent->referrer();

		$data	= [
			'id_user'	=> $this->session->userdata('id_user'),
			'id_kelas'	=> $this->input->post('id_kelas'),
			'rating'	=> $this->input->post('stars')
		];

		$insert	= $this->model_rating->insert($data);

		if ($insert) {
			echo "<script>
			alert('Rating Anda Telah Diinputkan, Terimakasih!');
			window.location.href='$prev';
			</script>";
		} else {
			echo "<script>
			alert('Ada Keasalahan Teknis Saat Input Rating Kelas!');
			window.location.href='$prev';
			</script>";
		}
	}
}
