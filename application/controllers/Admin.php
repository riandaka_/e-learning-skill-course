<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		// var_dump($this->session->userdata('is_login')); die;
		if (!$this->session->userdata('is_login'))
		{
			return redirect('login');
		}
	}

	public function kelas()
	{
		$data 		= $this->model_kelas->get();
		$instruktur = $this->model_instruktur->get();
		$kategori 	= $this->model_kategori->get();

		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('admin/kelas', compact('data', 'instruktur', 'kategori'));
		$this->load->view('layouts/footer');
	}

	public function insertKelas()
	{
		if (isset($_FILES['video']['name']) && $_FILES['video']['name'] != '' && isset($_FILES['thumbnail']['name']) && $_FILES['thumbnail']['name'] != '') {
			$date = date("ymd");
			unset($config);

			$config['upload_path'] = './assets/file-kelas';
			$config['max_size'] = '60000';
			$config['allowed_types'] = 'avi|flv|wmv|mp3|mp4|gif|jpg|png|jpeg';
			$config['overwrite'] = FALSE;
			$config['remove_spaces'] = TRUE;
			$video_name = $_FILES['video']['name'];
			$video_name = str_replace(' ', '_', $video_name);
			$thumbnail_name = $_FILES['thumbnail']['name'];
			$thumbnail_name = str_replace(' ', '_', $thumbnail_name);
			$config['file_name_video'] = $video_name;
			$config['file_name_thumbnail'] = $thumbnail_name;

			$this->load->library('upload', $config);

			for ($i=0; $i<2; $i++)
			{
				if ($i == 0)
				{
					if(!$this->upload->do_upload('video')) {
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('danger', "Data Kelas Gagal Ditambahkan, Kegagalan Sistem Saat Upload Video $error");
						return redirect('admin/kelas');
					}
				} else {
					if(!$this->upload->do_upload('thumbnail')) {
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('danger', "Data Kelas Gagal Ditambahkan, Kegagalan Sistem Saat Upload Thumbnail $error");
						return redirect('admin/kelas');
					}
				}
			}
			
				$videoDetails = $this->upload->data();
				$data['video_name']= $config['file_name_video'];
				$data['video_detail'] = $videoDetails;
				
				$thumbnailDetails = $this->upload->data();
				$data['thumbnail_name']= $config['file_name_thumbnail'];
				$data['thumbnail_detail'] = $thumbnailDetails;

				$dataInsert = [
					'nama_kelas'		=> $this->input->post('nama_kelas'),	
					'deskripsi_kelas'	=> $this->input->post('deskripsi_kelas'),
					'id_kategori'		=> $this->input->post('id_kategori'),
					'id_instruktur'		=> $this->input->post('id_instruktur'),
					'tag1'				=> $this->input->post('tag1'),
					'tag2'				=> $this->input->post('tag2'),
					'tag3'				=> $this->input->post('tag3'),
					'tag4'				=> $this->input->post('tag4'),
					'tag5'				=> $this->input->post('tag5'),
					'thumbnail'			=> $data['thumbnail_name'],
					'file_video'		=> $data['video_name'],
				]; 

				$insert = $this->model_kelas->insert($dataInsert);

				if ($insert)
				{
					$this->session->set_flashdata('success', 'Data Kelas Ditambahkan');
				} else 
				{
					$this->session->set_flashdata('danger', 'Data Kelas Gagal Ditambahkan');
				}
				return redirect('admin/kelas');
	
		}else{
			$this->session->set_flashdata('danger', 'Data Kelas Gagal Ditambahkan, Tolong Pilih Video Materi');
			return redirect('admin/kelas');
		}
	  }

	public function editKelas($id)
	{
		$instruktur = $this->model_instruktur->get();
		$kategori 	= $this->model_kategori->get();
		$data		= $this->model_kelas->show('id', $id)->row();

		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('admin/edit_kelas', compact('data', 'instruktur', 'kategori'));
		$this->load->view('layouts/footer');
	}

	public function updateKelas()
	{
		// var_dump($_FILES['video']['name']); die;
		if (isset($_FILES['video']['name']) && $_FILES['video']['name'] != '' && $_FILES['thumbnail']['name'] == '') {
			unset($config);
			$date = date("ymd");
			$configVideo['upload_path'] = './assets/file-kelas';
			$configVideo['max_size'] = '60000';
			$configVideo['allowed_types'] = 'avi|flv|wmv|mp3|mp4';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$video_name = $_FILES['video']['name'];
			$video_name = str_replace(' ', '_', $video_name);
			$video_name = str_replace('.', '_', $video_name);
			$configVideo['file_name'] = $video_name;

			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if(!$this->upload->do_upload('video')) {
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('danger', "Data Kelas Gagal Diperbarui, Kegagalan Sistem Saat Upload Video $error");
				return redirect('admin/kelas'); 
			}else{
				$videoDetails = $this->upload->data();
				$data['video_name']= $configVideo['file_name'];
				$data['video_detail'] = $videoDetails;
				$dataUpdate = [
					'nama_kelas'		=> $this->input->post('nama_kelas'),	
					'deskripsi_kelas'	=> $this->input->post('deskripsi_kelas'),
					'id_kategori'		=> $this->input->post('id_kategori'),
					'id_instruktur'		=> $this->input->post('id_instruktur'),
					'tag1'				=> $this->input->post('tag1'),
					'tag2'				=> $this->input->post('tag2'),
					'tag3'				=> $this->input->post('tag3'),
					'tag4'				=> $this->input->post('tag4'),
					'tag5'				=> $this->input->post('tag5'),
					'file_video'		=> $data['video_name'],
				]; 
				
			}

		} else if ($_FILES['video']['name'] == '' && isset($_FILES['thumbnail']['name']) && $_FILES['thumbnail']['name'] != '') {
			// print_r('aw'); die;
			unset($config);
			$date = date("ymd");
			$configThumbnail['upload_path'] = './assets/file-kelas';
			$configThumbnail['max_size'] = '60000';
			$configThumbnail['allowed_types'] = 'gif|jpg|png|jpeg';
			$configThumbnail['overwrite'] = FALSE;
			$configThumbnail['remove_spaces'] = TRUE;
			$thumbnail_name = $_FILES['thumbnail']['name'];
			$thumbnail_name = str_replace(' ', '_', $thumbnail_name);
			$thumbnail_name = str_replace('.', '_', $thumbnail_name);
			$configThumbnail['file_name'] = $thumbnail_name;
			$fileExt = pathinfo($_FILES["thumbnail"]["name"], PATHINFO_EXTENSION);

			$this->load->library('upload', $configThumbnail);
			$this->upload->initialize($configThumbnail);
			if(!$this->upload->do_upload('thumbnail')) {
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('danger', "Data Kelas Gagal Diperbarui, Kegagalan Sistem Saat Upload Thumbnail $error");
				return redirect('admin/kelas'); 
			}else{
				$thumbnailDetails = $this->upload->data();
				$data['thumbnail_name']= $configThumbnail['file_name'];
				$data['thumbnail_detail'] = $thumbnailDetails;
				$dataUpdate = [
					'nama_kelas'		=> $this->input->post('nama_kelas'),	
					'deskripsi_kelas'	=> $this->input->post('deskripsi_kelas'),
					'id_kategori'		=> $this->input->post('id_kategori'),
					'id_instruktur'		=> $this->input->post('id_instruktur'),
					'tag1'				=> $this->input->post('tag1'),
					'tag2'				=> $this->input->post('tag2'),
					'tag3'				=> $this->input->post('tag3'),
					'tag4'				=> $this->input->post('tag4'),
					'tag5'				=> $this->input->post('tag5'),
					'thumbnail'			=> $data['thumbnail_name'].'.'.$fileExt,
				]; 
				
			}
		} else if (isset($_FILES['video']['name']) && $_FILES['video']['name'] != '' && isset($_FILES['thumbnail']['name']) && $_FILES['thumbnail']['name'] != '') {
			$config['upload_path'] = './assets/file-kelas';
			$config['max_size'] = '60000';
			$config['allowed_types'] = 'avi|flv|wmv|mp3|mp4|gif|jpg|png|jpeg';
			$config['overwrite'] = FALSE;
			$config['remove_spaces'] = TRUE;
			$video_name = $_FILES['video']['name'];
			$video_name = str_replace(' ', '_', $video_name);
			$video_name = str_replace('.', '_', $video_name);
			$thumbnail_name = $_FILES['thumbnail']['name'];
			$thumbnail_name = str_replace(' ', '_', $thumbnail_name);
			$thumbnail_name = str_replace('.', '_', $thumbnail_name);
			$config['file_name_video'] = $video_name;
			$config['file_name_thumbnail'] = $thumbnail_name;

			$this->load->library('upload', $config);

			for ($i=0; $i<2; $i++)
			{
				if ($i == 0)
				{
					if(!$this->upload->do_upload('video')) {
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('danger', "Data Kelas Gagal Diperbarui, Kegagalan Sistem Saat Upload Video $error");
						return redirect('admin/kelas');
					}
				} else {
					if(!$this->upload->do_upload('thumbnail')) {
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('danger', "Data Kelas Gagal Diperbarui, Kegagalan Sistem Saat Upload Thumbnail $error");
						return redirect('admin/kelas');
					}
				}
			}
			
				$videoDetails = $this->upload->data();
				$data['video_name']= $config['file_name_video'];
				$data['video_detail'] = $videoDetails;
				
				$thumbnailDetails = $this->upload->data();
				$data['thumbnail_name']= $config['file_name_thumbnail'];
				$data['thumbnail_detail'] = $thumbnailDetails;

				$dataUpdate = [
					'nama_kelas'		=> $this->input->post('nama_kelas'),	
					'deskripsi_kelas'	=> $this->input->post('deskripsi_kelas'),
					'id_kategori'		=> $this->input->post('id_kategori'),
					'id_instruktur'		=> $this->input->post('id_instruktur'),
					'tag1'				=> $this->input->post('tag1'),
					'tag2'				=> $this->input->post('tag2'),
					'tag3'				=> $this->input->post('tag3'),
					'tag4'				=> $this->input->post('tag4'),
					'tag5'				=> $this->input->post('tag5'),
					'thumbnail'			=> $data['thumbnail_name'],
					'file_video'		=> $data['video_name'],
				]; 
		} else {
			// print_r('uw'); die;
			
			$dataUpdate = [
				'nama_kelas'		=> $this->input->post('nama_kelas'),	
				'deskripsi_kelas'	=> $this->input->post('deskripsi_kelas'),
				'id_kategori'		=> $this->input->post('id_kategori'),
				'id_instruktur'		=> $this->input->post('id_instruktur'),
				'tag1'				=> $this->input->post('tag1'),
				'tag2'				=> $this->input->post('tag2'),
				'tag3'				=> $this->input->post('tag3'),
				'tag4'				=> $this->input->post('tag4'),
				'tag5'				=> $this->input->post('tag5'),
			];
		}
		
		$update = $this->model_kelas->update($this->input->post('id'), $dataUpdate);

		if ($update)
		{
			$this->session->set_flashdata('success', 'Data Kelas Diperbarui');
		} else 
		{
			$this->session->set_flashdata('error', 'Data Kelas Gagal Diperbarui');
		}

		return redirect('admin/kelas');
	}

	public function deleteKelas($id)
	{
		$delete	= $this->model_kelas->delete($id);
		if ($delete)
		{
			$this->session->set_flashdata('success', 'Data Kelas Dihapus');
		} else 
		{
			$this->session->set_flashdata('success', 'Data Kelas Gagal Diperbarui');

		}
		return redirect('admin/kelas');
	}

	public function pesertaKelas($idKelas)
	{
		$kelas 		= $this->model_kelas->show('id', $idKelas)->row();
		$peserta 	= $this->model_peserta->showByIdKelas('id_kelas', $idKelas)->result_array();

		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('admin/peserta', compact('kelas', 'peserta'));
		$this->load->view('layouts/footer');
	}

	public function instruktur()
	{
		$data 		= $this->model_instruktur->get();

		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('admin/instruktur', compact('data'));
		$this->load->view('layouts/footer');
	}

	public function insertInstruktur()
	{
		$data	= [
			'email'			=> $this->input->post('email'),
			'password'		=> md5($this->input->post('password')),
			'nama_lengkap'	=> $this->input->post('nama_lengkap'),
			'tanggal_lahir'	=> $this->input->post('tanggal_lahir'),
			'role'			=> 'instruktur'
		];

		$insert	= $this->model_instruktur->insert($data);

		if ($insert) {
			$this->session->set_flashdata('success', 'Data Instruktur Sukses Diinputkan');
		} else {
			$this->session->set_flashdata('danger', 'Data Instruktur Gagal Diinputkan');
		}

		return redirect('admin/instruktur');
	}

	public function deleteInstruktur($id)
	{
		$delete	= $this->model_instruktur->delete($id);
		if ($delete)
		{
			$this->session->set_flashdata('success', 'Data Instruktur Sukses Dihapus');
		} else 
		{
			$this->session->set_flashdata('success', 'Data Instruktur Gagal Dihapus');

		}
		return redirect('admin/instruktur');
	}

	public function kategori()
	{
		$data 		= $this->model_kategori->get();

		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('admin/kategori', compact('data'));
		$this->load->view('layouts/footer');
	}

	public function insertKategori()
	{
		$data	= [
			'nama_kategori'	=> $this->input->post('nama_kategori'),
			'deskripsi'		=> $this->input->post('deskripsi'),
		];

		$insert	= $this->model_kategori->insert($data);

		if ($insert) {
			$this->session->set_flashdata('success', 'Data Kategori Sukses Diinputkan');
		} else {
			$this->session->set_flashdata('danger', 'Data Kategori Gagal Diinputkan');
		}

		return redirect('admin/kategori');
	}

	
	public function editKategori($id)
	{
		$data		= $this->model_kategori->show('id', $id)->row();
		// var_dump($data); die;
		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('admin/edit_kategori', compact('data'));
		$this->load->view('layouts/footer');
	}

	public function deleteKategori($id)
	{
		$delete	= $this->model_kategori->delete($id);
		if ($delete)
		{
			$this->session->set_flashdata('success', 'Data Kategori Sukses Dihapus');
		} else 
		{
			$this->session->set_flashdata('success', 'Data Kategori Gagal Dihapus');

		}
		return redirect('admin/kategori');
	}

	public function updateKategori()
	{
		$data	= [
			'nama_kategori'	=> $this->input->post('nama_kategori'),
			'deskripsi'		=> $this->input->post('deskripsi'),
		];

		$id = $this->input->post('id');

		$update	= $this->model_kategori->update($id, $data);

		if ($update) {
			$this->session->set_flashdata('success', 'Data Kategori Sukses Diperbarui');
		} else {
			$this->session->set_flashdata('danger', 'Data Kategori Gagal Diperbarui');
		}

		return redirect('admin/kategori');
	}

	public function mae()
	{
		$data	= $this->model_mae->get();

		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('admin/mae', compact('data'));
		$this->load->view('layouts/footer');
	}
}
