<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data 	= $this->model_kelas->get();
		
		$this->load->view('front/kelas', compact('data'));
	}
}
