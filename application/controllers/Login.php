<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		// var_dump($this->session->userdata('is_login') == 1); die;
		// if ($this->session->has_userdata('is_login') == 1)
		// {
		// 	return redirect('admin');
		// }
	}

	public function admin()
	{
		$this->load->view('login/admin'); 
	}

	public function instruktur()
	{
		$this->load->view('login/instruktur'); 
	}

	public function aksi()
	{
		$where = [
			'email'		=> $this->input->post('email'),
			'password'	=> md5($this->input->post('pass')),
			'role'		=> $this->input->post('role')
		];

		$check	= $this->model_login->check($where);
		// var_dump($check->num_rows()); die;

		if ($check->num_rows() == 1)
		{
			$data		= $check->row();
			$session 	= [
				'id_user'		=> $data->id,
				'email'			=> $data->email,
				'role'			=> $data->role,
				'nama_lengkap'	=> $data->nama_lengkap,
				'tanggal_lahir'	=> $data->tanggal_lahir,
				'is_login'		=> 1,
			];

			$this->session->set_userdata($session);
			$this->session->set_flashdata('success', 'Login Sukses');

		} else {
			$this->session->set_flashdata('danger', 'Login Gagal, Kombinasi Email Password Salah');
		}
		

		if ($session['role'] == 'peserta')
		{
			return redirect('welcome');
		} else if ($session['role'] == 'admin') {
			return redirect('admin/kelas');
		} else {
			return redirect('instruktur/kelas');
		}
	}


}
