<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$peserta	= $this->model_user->getByRole('peserta')->num_rows();
		$instruktur	= $this->model_user->getByRole('instruktur')->num_rows();
		$kelas		= $this->model_kelas->count()->num_rows();
		$dataKelas	= $this->model_kelas->get();
		$dataKategori	= $this->model_kategori->get();

		$this->load->view('front/index', compact('peserta', 'instruktur', 'kelas', 'dataKelas', 'dataKategori'));
	}
}
