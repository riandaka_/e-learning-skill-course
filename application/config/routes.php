<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['welcome'] = 'welcome';

$route['admin'] = 'login/admin';
$route['instruktur'] = 'login/instruktur';
$route['login'] = 'login/peserta';
$route['aksi-login'] = 'login/aksi';
$route['aksi-daftar'] = 'daftar/aksi';
$route['logout'] = 'logout';

$route['peserta'] = 'peserta';
$route['kelas']   = 'kelas';
$route['daftar-kelas'] = 'daftar/daftarKelas';
$route['kelas-saya'] = 'peserta/kelas';
$route['tonton-kelas'] = 'peserta/tontonKelas';
$route['submit-rating'] = 'peserta/submitRating';

$route['admin/kelas'] = 'admin/kelas';
$route['admin/insert-kelas'] = 'admin/insertKelas';
$route['admin/edit-kelas/(:any)'] = 'admin/editKelas/$1';
$route['admin/update-kelas'] = 'admin/updateKelas';
$route['admin/delete-kelas/(:any)'] = 'admin/deleteKelas/$1';
$route['admin/peserta-kelas/(:any)'] = 'admin/pesertaKelas/$1';
$route['admin/instruktur'] = 'admin/instruktur';
$route['admin/insert-instruktur'] = 'admin/insertInstruktur';
$route['admin/delete-instruktur/(:any)'] = 'admin/deleteInstruktur/$1';
$route['admin/kategori'] = 'admin/kategori';
$route['admin/insert-kategori'] = 'admin/insertKategori';
$route['admin/edit-kategori/(:any)'] = 'admin/editKategori/$1';
$route['admin/update-kategori'] = 'admin/updateKategori';
$route['admin/delete-kategori/(:any)'] = 'admin/deleteKategori/$1';

$route['instruktur/kelas'] = 'instruktur/kelas';
$route['instruktur/insert-kelas'] = 'instruktur/insertKelas';
$route['instruktur/edit-kelas/(:any)'] = 'instruktur/editKelas/$1';
$route['instruktur/update-kelas'] = 'instruktur/updateKelas';
$route['instruktur/delete-kelas/(:any)'] = 'instruktur/deleteKelas/$1';
$route['instruktur/peserta-kelas/(:any)'] = 'instruktur/pesertaKelas/$1';


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
