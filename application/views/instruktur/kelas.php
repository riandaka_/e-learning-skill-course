<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Kelas Saya</h1>
<?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
<?php } else if ($this->session->flashdata('warning')) { ?>
        <div class="alert alert-warning"> <?= $this->session->flashdata('warning') ?> </div>
<?php } else if ($this->session->flashdata('info')) { ?>
        <div class="alert alert-info"> <?= $this->session->flashdata('info') ?> </div>
<?php } else if ($this->session->flashdata('danger')) { ?>
        <div class="alert alert-danger"> <?= $this->session->flashdata('danger') ?> </div>
<?php } ?>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <button class="btn btn-primary" data-toggle="modal" data-target="#modalAdd" id="btnAdd">+ Tambah Kelas</button>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered display nowrap" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><center>No</center></th>
            <th><center>Nama Kelas</center></th>
            <th><center>Tags</center></th>
            <th><center><i class="fa fa-cogs"></i></center></th>
          </tr>
        </thead>
          <tbody>
          <?php
          $no = 0;
          foreach ($data as $el) {
              $no++; ?>
          <tr>
              <td width="10"><center><b><?= $no; ?></b></center></td>
              <td width="20"><center><b><?= $el['nama_kelas']; ?></b></center></td>
              <td width="20"><center><b><?= $el['tag1'].','.$el['tag2'].','.$el['tag3'].','.$el['tag4'].','.$el['tag5']; ?></b></center></td>
              <td width="20"><center>
                <button id="btnDetail" class='btn btn-info' data-deskripsi="<?=$el['deskripsi_kelas'];?>" data-video="<?=$el['file_video'];?>"><i class='fas fa-eye'></i></button>
                <a href='<?= base_url();?>admin/edit-kelas/<?= $el['id_kelas'];?>' class='btn btn-warning'><i class='fas fa-pen'></i></a>
                <a href='<?= base_url();?>admin/delete-kelas/<?= $el['id_kelas'];?>' class='btn btn-danger'><i class='fa fa-trash'></i></a>
                <a href='<?= base_url();?>admin/peserta-kelas/<?= $el['id_kelas'];?>' class='btn btn-success'><i class='fa fa-users'></i></a>
              </center></td>
          </tr>
              <?php } ?>
          </tbody>
      </table>
    </div>
  </div>
</div>

</div>

<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Kelas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="<?=base_url();?>instruktur/insert-kelas" enctype="multipart/form-data">
          <label for="">Nama Kelas</label><br>
          <input required class="form-control" type="text" name="nama_kelas"><br>

          <label for="">Deskripsi Kelas</label><br>
          <textarea name="deskripsi_kelas" required id="" class="form-control" cols="30" rows="10"></textarea><br>

          <label for="">Tag 1</label><br>
          <input required class="form-control" type="text" name="tag1"><br>

          <label for="">Tag 2</label><br>
          <input required class="form-control" type="text" name="tag2"><br>

          <label for="">Tag 3</label><br>
          <input required class="form-control" type="text" name="tag3"><br>

          <label for="">Tag 4</label><br>
          <input required class="form-control" type="text" name="tag4"><br>

          <label for="">Tag 5</label><br>
          <input required class="form-control" type="text" name="tag5"><br>

          <label for="">File Video</label><br>
          <input required class="form-control" type="file" name="video"><br>
          
          <label for="">Thumbnail</label><br>
          <input required class="form-control" type="file" name="thumbnail"><br>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Deskripsi Dan Video</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <label for="">Deskripsi Kelas</label><br>
        <textarea id="deskripsi_kelas" required id="" class="form-control" cols="30" rows="10"></textarea><br>
        
        <video controls id="file_video" type="video/mp4" style="width:760px" src=""></video>
      </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready( function () {

    $('#dataTable').DataTable({
      "scrollX" : true
    });

$(document).on('click', '#btnDetail', function(){
  var deskripsi = $(this).data('deskripsi')
  var video     = $(this).data('video')
  var getUrl    = window.location;
  var baseUrl   = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
  var pathVideo = baseUrl+'/assets/video-kelas/'+video

  console.log(pathVideo)
  $('#deskripsi_kelas').val(deskripsi)
  $('#file_video').attr('src', pathVideo)
  $("#file_video")[0].load()

  $('#modalDetail').modal('show')
})

} );
</script>