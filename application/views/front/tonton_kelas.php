<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>E-Learning By Riandaka</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by freehtml5.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="freehtml5.co" />

	<!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FreeHTML5.co
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/owl.theme.default.min.css">

	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/flexslider.css">

	<!-- Pricing -->
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/pricing.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/style.css">

	<!-- Modernizr JS -->
	<script src="<?=base_url();?>/assets/main/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="<?=base_url();?>/assets/main/js/respond.min.js"></script>
	<![endif]-->
	<link href="<?= base_url();?>assets/dashboard/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	</head>
	<body>
	<style>
	.checked {
	color: orange;
	}

	.rating {
	display: inline-block;
	position: relative;
	height: 50px;
	line-height: 50px;
	font-size: 50px;
	}

	.rating label {
	position: absolute;
	top: 0;
	left: 0;
	height: 100%;
	cursor: pointer;
	}

	.rating label:last-child {
	position: static;
	}

	.rating label:nth-child(1) {
	z-index: 5;
	}

	.rating label:nth-child(2) {
	z-index: 4;
	}

	.rating label:nth-child(3) {
	z-index: 3;
	}

	.rating label:nth-child(4) {
	z-index: 2;
	}

	.rating label:nth-child(5) {
	z-index: 1;
	}

	.rating label input {
	position: absolute;
	top: 0;
	left: 0;
	opacity: 0;
	}

	.rating label .icon {
	float: left;
	color: transparent;
	}

	.rating label:last-child .icon {
	color: #000;
	}

	.rating:not(:hover) label input:checked ~ .icon,
	.rating:hover label:hover input ~ .icon {
	color: orange;
	}

	.rating label input:focus:not(:checked) ~ .icon:last-child {
	color: #000;
	text-shadow: 0 0 5px #09f;
	}

	</style>
		
	<div class="fh5co-loader"></div>
	
	<div id="page">
	<nav class="fh5co-nav" role="navigation">
		<div class="top">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-right">
					<?php if ($this->session->userdata('is_login')) { ?>
						<p class="site"><?= $this->session->userdata('nama_lengkap');?></p>
					<?php } else { ?>
						<p id="status-login" class="site">Anda belum login</p>
						<?php } ?>
						<p class="num">+62 819-0781-9253</p>
						<ul class="fh5co-social">
							<li><a href="#"><i class="icon-facebook2"></i></a></li>
							<li><a href="#"><i class="icon-twitter2"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="top-menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-2">
						<div id="fh5co-logo"><a href="index.html"><i class="icon-study"></i>E-Learning By Riandaka<span>.</span></a></div>
					</div>
					<div class="col-xs-10 text-right menu-1">
						<ul>
							<li><a href="<?=base_url();?>">Home</a></li>
							<li><a href="<?=base_url();?>kelas">Kelas</a></li>
							<?php if ($this->session->userdata('is_login')) { ?>
								<li class="active"><a href="<?=base_url();?>kelas-saya">Kelas Saya</a></li>
							<?php } ?>
							<!-- <li><a href="teacher.html">Teacher</a></li> -->
							<li><a href="about.html">About</a></li>
							<!-- <li><a href="pricing.html">Pricing</a></li> -->
							<!-- <li class="has-dropdown">
								<a href="blog.html">Blog</a>
								<ul class="dropdown">
									<li><a href="#">Web Design</a></li>
									<li><a href="#">eCommerce</a></li>
									<li><a href="#">Branding</a></li>
									<li><a href="#">API</a></li>
								</ul>
							</li> -->
							<li><a href="contact.html">Contact</a></li>
							<?php if (!$this->session->userdata('is_login')) { ?>
							<li class=""><button class="btn btn-primary" data-toggle="modal" data-target="#modalLogin"><span>Login Peserta</span></button></li>
							<li class=""><button class="btn btn-danger" data-toggle="modal" data-target="#modalDaftar"><span>Daftar Peserta</span></button></li>
							<li class="btn btn-md btn-success"><a href="<?= base_url();?>instruktur" style="color:white"><b>Login Instruktur</b></a></li>
							<?php } else { ?>
							<li class="btn btn-md btn-primary"><a style="color:white" href="<?= base_url();?>logout"><b>Logout</b></a></li>
							<?php  } ?>
						</ul>
					</div>
					<div class="col-xs-10 text-right menu-1">

					</div>
				</div>
				
			</div>
		</div>
	</nav>
	
	<div id="fh5co-blog">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h1><?=$kelas->nama_kelas;?></h1>
				</div>
			</div>
			<div class="text-center">
				<video controls id="video" type="video/mp4" style="width:1000px" src="<?=base_url();?>assets/file-kelas/<?=$kelas->file_video;?>"></video><br>
				<?php if ($check->num_rows() == 0) { ?>
					<br><button type="button" data-toggle="modal" data-target="#modalRating" class="btn btn-lg btn-success"><i aria-hidden="true" class="fas fa-star"></i> Beri Rating Kelas Ini</button>
				<?php } else { ?>
					<br><button class="btn btn-success">Anda Telah Memberi Rating Kelas Ini</button><br>
					<?php for ($i=0; $i < $check->row()->rating; $i++)
					{ ?>
						<span class="fas fa-star fa-3x checked"></span>
					<?php } ?>
				<?php } ?>
			</div>

		</div>
	</div>

	<div class="modal fade" id="modalRating" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Beri Rating Untuk Kelas <?=$kelas->nama_kelas;?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center">
				<form method="POST" action="<?=base_url();?>submit-rating" class="rating">
					<input type="hidden" name="id_kelas" value="<?=$kelas->id;?>">
					<label>
						<input type="radio" name="stars" value="1" />
						<span class="icon">★</span>
					</label>
					<label>
						<input type="radio" name="stars" value="2" />
						<span class="icon">★</span>
						<span class="icon">★</span>
					</label>
					<label>
						<input type="radio" name="stars" value="3" />
						<span class="icon">★</span>
						<span class="icon">★</span>
						<span class="icon">★</span>   
					</label>
					<label>
						<input type="radio" name="stars" value="4" />
						<span class="icon">★</span>
						<span class="icon">★</span>
						<span class="icon">★</span>
						<span class="icon">★</span>
					</label>
					<label>
						<input type="radio" name="stars" value="5" />
						<span class="icon">★</span>
						<span class="icon">★</span>
						<span class="icon">★</span>
						<span class="icon">★</span>
						<span class="icon">★</span>
					</label>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Submit Rating</button>
			</div>
			</div>
				</form>
		</div>
	</div>

	<footer id="fh5co-footer" role="contentinfo" style="background-image: url(images/img_bg_4.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-3 fh5co-widget">
					<h3>About Education</h3>
					<p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit. Eos cumque dicta adipisci architecto culpa amet.</p>
				</div>
				<div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1 fh5co-widget">
					<h3>Learning</h3>
					<ul class="fh5co-footer-links">
						<li><a href="#">Course</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Contact</a></li>
						<li><a href="#">Terms</a></li>
						<li><a href="#">Meetups</a></li>
					</ul>
				</div>

				<div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1 fh5co-widget">
					<h3>Learn &amp; Grow</h3>
					<ul class="fh5co-footer-links">
						<li><a href="#">Blog</a></li>
						<li><a href="#">Privacy</a></li>
						<li><a href="#">Testimonials</a></li>
						<li><a href="#">Handbook</a></li>
						<li><a href="#">Held Desk</a></li>
					</ul>
				</div>

				<div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1 fh5co-widget">
					<h3>Engage us</h3>
					<ul class="fh5co-footer-links">
						<li><a href="#">Marketing</a></li>
						<li><a href="#">Visual Assistant</a></li>
						<li><a href="#">System Analysis</a></li>
						<li><a href="#">Advertise</a></li>
					</ul>
				</div>

				<div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1 fh5co-widget">
					<h3>Legal</h3>
					<ul class="fh5co-footer-links">
						<li><a href="#">Find Designers</a></li>
						<li><a href="#">Find Developers</a></li>
						<li><a href="#">Teams</a></li>
						<li><a href="#">Advertise</a></li>
						<li><a href="#">API</a></li>
					</ul>
				</div>
			</div>

			<div class="row copyright">
				<div class="col-md-12 text-center">
					<p>
						<small class="block">&copy; 2016 Free HTML5. All Rights Reserved.</small> 
						<small class="block">Designed by <a href="http://freehtml5.co/" target="_blank">FreeHTML5.co</a> Demo Images: <a href="http://unsplash.co/" target="_blank">Unsplash</a> &amp; <a href="https://www.pexels.com/" target="_blank">Pexels</a></small>
					</p>
				</div>
			</div>

		</div>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="<?=base_url();?>/assets/main/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?=base_url();?>/assets/main/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?=base_url();?>/assets/main/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?=base_url();?>/assets/main/js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="<?=base_url();?>/assets/main/js/jquery.stellar.min.js"></script>
	<!-- Carousel -->
	<script src="<?=base_url();?>/assets/main/js/owl.carousel.min.js"></script>
	<!-- Flexslider -->
	<script src="<?=base_url();?>/assets/main/js/jquery.flexslider-min.js"></script>
	<!-- countTo -->
	<script src="<?=base_url();?>/assets/main/js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="<?=base_url();?>/assets/main/js/jquery.magnific-popup.min.js"></script>
	<script src="<?=base_url();?>/assets/main/js/magnific-popup-options.js"></script>
	<!-- Count Down -->
	<script src="<?=base_url();?>/assets/main/js/simplyCountdown.js"></script>
	<!-- Main -->
	<script src="<?=base_url();?>/assets/main/js/main.js"></script>
	<script>
    var d = new Date(new Date().getTime() + 1000 * 120 * 120 * 2000);

    // default example
    simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
    });

    //jQuery example
    $('#simply-countdown-losange').simplyCountdown({
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        enableUtc: false
    });

	$(':radio').change(function() {
		console.log('New star rating: ' + this.value);
	});
	</script>
	</body>
</html>

