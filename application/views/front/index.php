<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>E-Learning By Riandaka</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by freehtml5.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="freehtml5.co" />

	<!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FreeHTML5.co
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/owl.theme.default.min.css">

	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/flexslider.css">

	<!-- Pricing -->
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/pricing.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="<?=base_url();?>/assets/main/css/style.css">

	<!-- Modernizr JS -->
	<script src="<?=base_url();?>/assets/main/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="<?=base_url();?>/assets/main/js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="fh5co-loader"></div>
	
	<div id="page">
	<nav class="fh5co-nav" role="navigation">
		<div class="top">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-right">
					<?php if ($this->session->userdata('is_login')) { ?>
						<p class="site"><?= $this->session->userdata('nama_lengkap');?></p>
					<?php } else { ?>
						<p id="status-login" class="site">Anda belum login</p>
						<?php } ?>
						<p class="num">+62 819-0781-9253</p>
						<ul class="fh5co-social">
							<li><a href="#"><i class="icon-facebook2"></i></a></li>
							<li><a href="#"><i class="icon-twitter2"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="top-menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-2">
						<div id="fh5co-logo"><a href="<?php base_url(); ?>"><i class="icon-study"></i>E-Learning By Riandaka<span>.</span></a></div>
					</div>
					<div class="col-xs-10 text-right menu-1">
						<ul>
							<li class="active"><a href="<?php base_url(); ?>">Home</a></li>
							<li><a href="<?=base_url();?>kelas">Kelas</a></li>
							<?php if ($this->session->userdata('is_login')) { ?>
								<li><a href="<?=base_url();?>kelas-saya">Kelas Saya</a></li>
							<?php } ?>
							<!-- <li><a href="teacher.html">Teacher</a></li> -->
							<!-- <li><a href="about.html">About</a></li> -->
							<!-- <li><a href="pricing.html">Pricing</a></li> -->
							<!-- <li class="has-dropdown">
								<a href="blog.html">Blog</a>
								<ul class="dropdown">
									<li><a href="#">Web Design</a></li>
									<li><a href="#">eCommerce</a></li>
									<li><a href="#">Branding</a></li>
									<li><a href="#">API</a></li>
								</ul>
							</li> -->
							<!-- <li><a href="contact.html">Contact</a></li> -->
							<?php if (!$this->session->userdata('is_login')) { ?>
							<li class=""><button class="btn btn-primary" data-toggle="modal" data-target="#modalLogin"><span>Login Peserta</span></button></li>
							<li class=""><button class="btn btn-danger" data-toggle="modal" data-target="#modalDaftar"><span>Daftar Peserta</span></button></li>
							<li class="btn btn-md btn-success"><a href="<?= base_url();?>instruktur" style="color:white"><b>Login Instruktur</b></a></li>
							<?php } else { ?>
							<li class="btn btn-md btn-primary"><a style="color:white" href="<?= base_url();?>logout"><b>Logout</b></a></li>
							<?php  } ?>
						</ul>
					</div>
					<div class="col-xs-10 text-right menu-1">

					</div>
				</div>
				
			</div>
		</div>
	</nav>

<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <center><h5 class="modal-title" id="exampleModalLabel">Login Peserta</h5></center>
		<center><h6 class="modal-title" id="exampleModalLabel">Silakan Masukkan Username dan Password</h6></center>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url();?>aksi-login" method="POST">
			<label for="">Email</label><br>
			<input type="email" class="form-control" name="email"><br>
			<label for="">Password</label><br>
			<input type="password" class="form-control" name="pass"><br>
			<input type="hidden" class="form-control" name="role" value="peserta"><br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Login</button>
      </div>
	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="modalDaftarKelas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Daftar Kelas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url();?>daftar-kelas" method="POST">
			<input type="hidden" name="id_kelas" id="id_kelas">
			<h5 id="confirm-kelas"></h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Ya</button>
      </div>
	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="modalDaftar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <center><h5 class="modal-title" id="exampleModalLabel">Daftar Peserta</h5></center>
		<center><h6 class="modal-title" id="exampleModalLabel">Silakan Masukkan Data Diri Anda</h6></center>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url();?>aksi-daftar" method="POST">
			<label for="">Email</label><br>
			<input required type="email" class="form-control" name="email"><br>
			<label for="">Password</label><br>
			<input required type="password" class="form-control" name="pass"><br>
			<label for="">Nama Lengkap</label><br>
			<input required type="text" class="form-control" name="nama_lengkap"><br>
			<label for="">Tanggal Lahir</label><br>
			<input required type="date" class="form-control" name="tanggal_lahir"><br>
			<input type="hidden" class="form-control" name="role" value="peserta"><br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Daftar</button>
      </div>
	</form>
    </div>
  </div>
</div>
	
	<aside id="fh5co-hero">
		<div class="flexslider">
			<ul class="slides">
		   	<li style="background-image: url(<?=base_url();?>/assets/main/images/bg1.png);">
		   		<div class="overlay-gradient"></div>
		   		<div class="container">
		   			<div class="row">
			   			<div class="col-md-8 col-md-offset-2 text-center slider-text">
			   				<div class="slider-text-inner">
			   					<h1>The Roots of Education are Bitter, But the Fruit is Sweet</h1>
									<p><a class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modalLogin">Start Learning Now!</a></p>
			   				</div>
			   			</div>
			   		</div>
		   		</div>
		   	</li>
		   	<li style="background-image: url(<?=base_url();?>/assets/main/images/bg2.png);">
		   		<div class="overlay-gradient"></div>
		   		<div class="container">
		   			<div class="row">
			   			<div class="col-md-8 col-md-offset-2 text-center slider-text">
			   				<div class="slider-text-inner">
			   					<h1>The Great Aim of Education is not Knowledge, But Action</h1>
									<p><a class="btn btn-primary btn-lg btn-learn" data-toggle="modal" data-target="#modalLogin">Start Learning Now!</a></p>
			   				</div>
			   			</div>
			   		</div>
		   		</div>
		   	</li>
		   	<li style="background-image: url(<?=base_url();?>/assets/main/images/bg3.png);">
		   		<div class="overlay-gradient"></div>
		   		<div class="container">
		   			<div class="row">
			   			<div class="col-md-8 col-md-offset-2 text-center slider-text">
			   				<div class="slider-text-inner">
			   					<h1>We Help You to Learn New Things</h1>
									<p><a class="btn btn-primary btn-lg btn-learn" data-toggle="modal" data-target="#modalLogin">Start Learning Now!</a></p>
			   				</div>
			   			</div>
			   		</div>
		   		</div>
		   	</li>		   	
		  	</ul>
	  	</div>
	</aside>

	<div id="fh5co-course-categories">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
					<h2>Kategori Kelas</h2>
					<p>Pilih kelas berdasarkan kategori kategori nya.</p>
				</div>
			</div>
			<div class="row">
				<?php foreach($dataKategori as $kategori) { ?>
				<div class="col-md-3 col-sm-6 text-center animate-box">
					<div class="services">
						<span class="icon">
							<i class="<?= $kategori['icon'];?>"></i>
						</span>
						<div class="desc">
							<h3><a href="#"><?= $kategori['nama_kategori'];?></a></h3>
							<p><?= $kategori['deskripsi'];?>.</p>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>

	<div id="fh5co-counter" class="fh5co-counters" style="background-image: url(<?=base_url();?>/assets/main/images/img_bg_4.jpg);" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="row">
						<div class="col-md-4 col-sm-6 text-center animate-box">
							<span class="icon"><i class="icon-study"></i></span>
							<span class="fh5co-counter js-counter" data-from="0" data-to="<?=$peserta;?>" data-speed="2000" data-refresh-interval="50"></span>
							<span class="fh5co-counter-label">Peserta Terdaftar</span>
						</div>
						<div class="col-md-4 col-sm-6 text-center animate-box">
							<span class="icon"><i class="icon-bulb"></i></span>
							<span class="fh5co-counter js-counter" data-from="0" data-to="<?=$kelas;?>" data-speed="2000" data-refresh-interval="50"></span>
							<span class="fh5co-counter-label">Kelas</span>
						</div>
						<div class="col-md-4 col-sm-6 text-center animate-box">
							<span class="icon"><i class="icon-head"></i></span>
							<span class="fh5co-counter js-counter" data-from="0" data-to="<?=$instruktur;?>" data-speed="2000" data-refresh-interval="50"></span>
							<span class="fh5co-counter-label">Instruktur</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-course">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
					<h2>Kelas</h2>
					<p>Pilih Kelas Sesukamu</p>
				</div>
			</div>
			<div class="row">
				<?php
					for ($i=0; $i<6; $i++){ ?>
						<div class="col-md-6 animate-box">
							<div class="course">
								<a href="#" class="course-img" style="background-image: url(<?=base_url();?>/assets/file-kelas/<?=$dataKelas[$i]['thumbnail'];?>);">
								</a>
								<div class="desc">
									<h3><a href="#"><?=$dataKelas[$i]['nama_kelas'];?></a></h3>
									<p><?=$dataKelas[$i]['deskripsi_kelas'];?></p>
									<span><a href="#" id="btn-daftar" data-id="<?=$dataKelas[$i]['id_kelas'];?>" data-nama="<?=$dataKelas[$i]['nama_kelas'];?>" class="btn btn-primary btn-sm btn-course">Daftar Kelas</a></span>
								</div>
							</div>
						</div>
				<?php } ?>
			</div>
			<center><a class="btn btn-primary btn-lg btn-learn" href="<?=base_url();?>kelas">Lihat Kelas Lainnya</a></center>
		</div>
	</div>

	<footer id="fh5co-footer" role="contentinfo" style="background-image: url(<?=base_url();?>/assets/main/images/img_bg_4.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-3 fh5co-widget">
					<h3>About Education</h3>
					<p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit. Eos cumque dicta adipisci architecto culpa amet.</p>
				</div>
				<div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1 fh5co-widget">
					<h3>Learning</h3>
					<ul class="fh5co-footer-links">
						<li><a href="#">Course</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Contact</a></li>
						<li><a href="#">Terms</a></li>
						<li><a href="#">Meetups</a></li>
					</ul>
				</div>

				<div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1 fh5co-widget">
					<h3>Learn &amp; Grow</h3>
					<ul class="fh5co-footer-links">
						<li><a href="#">Blog</a></li>
						<li><a href="#">Privacy</a></li>
						<li><a href="#">Testimonials</a></li>
						<li><a href="#">Handbook</a></li>
						<li><a href="#">Held Desk</a></li>
					</ul>
				</div>

				<div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1 fh5co-widget">
					<h3>Engage us</h3>
					<ul class="fh5co-footer-links">
						<li><a href="#">Marketing</a></li>
						<li><a href="#">Visual Assistant</a></li>
						<li><a href="#">System Analysis</a></li>
						<li><a href="#">Advertise</a></li>
					</ul>
				</div>

				<div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1 fh5co-widget">
					<h3>Legal</h3>
					<ul class="fh5co-footer-links">
						<li><a href="#">Find Designers</a></li>
						<li><a href="#">Find Developers</a></li>
						<li><a href="#">Teams</a></li>
						<li><a href="#">Advertise</a></li>
						<li><a href="#">API</a></li>
					</ul>
				</div>
			</div>

			<div class="row copyright">
				<div class="col-md-12 text-center">
					<p>
						<small class="block">&copy; 2016 Free HTML5. All Rights Reserved.</small> 
						<small class="block">Designed by <a href="http://freehtml5.co/" target="_blank">FreeHTML5.co</a> Demo Images: <a href="http://unsplash.co/" target="_blank">Unsplash</a> &amp; <a href="https://www.pexels.com/" target="_blank">Pexels</a></small>
					</p>
				</div>
			</div>

		</div>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="<?=base_url();?>/assets/main/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?=base_url();?>/assets/main/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?=base_url();?>/assets/main/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?=base_url();?>/assets/main/js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="<?=base_url();?>/assets/main/js/jquery.stellar.min.js"></script>
	<!-- Carousel -->
	<script src="<?=base_url();?>/assets/main/js/owl.carousel.min.js"></script>
	<!-- Flexslider -->
	<script src="<?=base_url();?>/assets/main/js/jquery.flexslider-min.js"></script>
	<!-- countTo -->
	<script src="<?=base_url();?>/assets/main/js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="<?=base_url();?>/assets/main/js/jquery.magnific-popup.min.js"></script>
	<script src="<?=base_url();?>/assets/main/js/magnific-popup-options.js"></script>
	<!-- Count Down -->
	<script src="<?=base_url();?>/assets/main/js/simplyCountdown.js"></script>
	<!-- Main -->
	<script src="<?=base_url();?>/assets/main/js/main.js"></script>
	<script>
    var d = new Date(new Date().getTime() + 1000 * 120 * 120 * 2000);

    // default example
    simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
    });

    //jQuery example
    $('#simply-countdown-losange').simplyCountdown({
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        enableUtc: false
    });

	$(document).ready(function (){
		$(document).on('click', '#btn-daftar', function(){
			var statusLogin	= $('#status-login').text()
			if (statusLogin == 'Anda belum login') {
				alert('Anda belum login, silahkan login terlebih dahulu')
				$('#modalLogin').modal('show')
			} else {
				var idKelas		= $(this).data('id')
				var namaKelas 	= $(this).data('nama')

				$('#modalDaftarKelas').modal('show')
				var confirm		= 'Anda Yakin Akan Ambil Kelas '+namaKelas+'?'
				$('#confirm-kelas').text(confirm)
				$('#id_kelas').val(idKelas)
			}
		})
	})
	</script>
	</body>
</html>

