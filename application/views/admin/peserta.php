<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Daftar Peserta Kelas <?= $kelas->nama_kelas; ?></h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered display nowrap" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><center>No</center></th>
            <th><center>Nama Lengkap</center></th>
            <th><center>Tanggal Lahir</center></th>
          </tr>
        </thead>
          <tbody>
          <?php
          $no = 0;
          foreach ($peserta as $el) {
              $no++; ?>
          <tr>
              <td width="10"><center><b><?= $no; ?></b></center></td>
              <td width="20"><center><b><?= $el['nama_lengkap']; ?></b></center></td>
              <td width="20"><center><b><?= $el['tanggal_lahir']; ?></b></center></td>
          </tr>
              <?php } ?>
          </tbody>
      </table>
    </div>
  </div>
</div>

</div>

<!-- /.container-fluid -->
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready( function () {

    $('#dataTable').DataTable({
      "scrollX" : true
    });


} );
</script>