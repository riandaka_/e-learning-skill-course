<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Edit Kelas</h1>

<div class="row justify-content-center">

      <div class="col-xl-11 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                  </div><br>

                  <form method="POST" action="<?php echo base_url('admin/update-kelas') ?>" enctype="multipart/form-data">
                    <input type="hidden" class="form-control" name="id" value="<?=$data->id;?>">

                    <div class="form-group">
                      <label for="">Nama Kelas</label>
                      <input type="text" class="form-control" name="nama_kelas" value="<?=$data->nama_kelas;?>">
                    </div>

                    <div class="form-group">
                      <label for="">Deskripsi Kelas</label>
                      <textarea type="text" class="form-control" cols="30" rows="10" name="deskripsi_kelas"><?=$data->deskripsi_kelas;?></textarea>
                    </div>

                    <div class="form-group">
                      <label for="">Kategori</label><br>
                      <select class="form-control" name="id_kategori">
                          <?php foreach($kategori as $el) {
                            if ($el['id'] == $data->id_kategori) { ?>
                          <option class="form-control" selected value="<?=$el['id'];?>"><?=$el['nama_kategori'];?></option>
                          <?php } else { ?>
                          <option class="form-control" value="<?=$el['id'];?>"><?=$el['nama_kategori'];?></option>
                            <?php } } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Nama Instruktur</label>
                        <select readonly class="form-control" name="id_instruktur">
                          <?php foreach($instruktur as $el) {
                            if ($el['id'] == $data->id_instruktur) { ?>
                          <option class="form-control" selected value="<?=$el['id'];?>"><?=$el['nama_lengkap'];?></option>
                          <?php } else { ?>
                          <option class="form-control" value="<?=$el['id'];?>"><?=$el['nama_lengkap'];?></option>
                            <?php } } ?>
                        </select>
                    </div>

                    <div class="form-group">
                      <label for="">Tag 1</label>
                      <input type="text" class="form-control" name="tag1" value="<?=$data->tag1;?>">
                    </div>

                    <div class="form-group">
                      <label for="">Tag 2</label>
                      <input type="text" class="form-control" name="tag2" value="<?=$data->tag2;?>">
                    </div>

                    <div class="form-group">
                      <label for="">Tag 3</label>
                      <input type="text" class="form-control" name="tag3" value="<?=$data->tag3;?>">
                    </div>

                    <div class="form-group">
                      <label for="">Tag 4</label>
                      <input type="text" class="form-control" name="tag4" value="<?=$data->tag4;?>">
                    </div>

                    <div class="form-group">
                      <label for="">Tag 5</label>
                      <input type="text" class="form-control" name="tag5" value="<?=$data->tag5;?>">
                    </div>

                    <div class="form-group">
                      <label for="">File Video</label>
                      <video controls id="file_video" type="video/mp4" style="width:825px" src="<?=base_url();?>/assets/file-kelas/<?=$data->file_video;?>"></video>
                      <input type="file" class="form-control" name="video">
                    </div>
               
                    <div class="form-group">
                      <label for="">Thumbnail</label><br>
                      <img id="thumbnail" style="width:400px;height:200px" src="<?=base_url();?>/assets/file-kelas/<?=$data->thumbnail;?>"><br>
                      <input type="file" class="form-control" name="thumbnail">
                    </div>
                    
                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary btn-block">Update</button>                        
                  </div>
                  </form>
                  <hr>                  
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

</div>
<!-- /.container-fluid -->