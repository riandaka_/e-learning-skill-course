<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Edit Kategori</h1>

<div class="row justify-content-center">

      <div class="col-xl-11 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                  </div><br>

                  <form method="POST" action="<?php echo base_url('admin/update-kategori') ?>" enctype="multipart/form-data">
                    <input type="hidden" class="form-control" name="id" value="<?=$data->id;?>">

                    <div class="form-group">
                      <label for="">Nama Kategori</label>
                      <input type="text" class="form-control" name="nama_kategori" value="<?=$data->nama_kategori;?>">
                    </div>

                    <div class="form-group">
                      <label for="">Deskripsi Kategori</label><br>
                      <textarea required class="form-control" type="text" name="deskripsi"><?=$data->deskripsi;?></textarea><br>
                    </div>

                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary btn-block">Update</button>                        
                  </div>
                  </form>
                  <hr>                  
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

</div>
<!-- /.container-fluid -->