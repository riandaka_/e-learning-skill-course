<?php
    
class Model_mae extends CI_Model {

    public function update($id, $mae)
    {
        $cek    =  $this->db->get_where('mae', ['id_user' => $id])->num_rows();
        if ($cek != 0) {
            $data   = [
                'mae'   => $mae
            ];

            return $this->db->update('mae', $data);
        } else {
            $data   = [
                'id_user'   => $id,
                'mae'       => $mae
            ];

            return $this->db->insert('mae', $data);
        }

    }

    public function get()
    {
        $this->db->select('A.*, B.*');
        $this->db->join('users B', 'B.id = A.id_user');
        $this->db->from('mae A');
        $data   = $this->db->get()->result_array();

        $this->db->select('SUM(mae) as sum_mae');
        $mae    = $this->db->get('mae')->row();

        $resp   = [
            'data'  => $data,
            'sum'   => $mae
        ];

        return $resp;

    }
}
