<?php
    
class Model_user extends CI_Model {

    public function getByRole($role)
    {
        return $this->db->get_where('users', ['role' => $role]);
    }

    public function insert($data)
    {
        return $this->db->insert('users', $data);
    }
}
