<?php
    
class Model_peserta extends CI_Model {

    public function getByPeserta($idPeserta)
    {
        $this->db->join('users b', 'b.id = a.id_user');
        $this->db->join('kelas c', 'c.id = a.id_kelas');
        $this->db->select('a.*, a.id as id_peserta, b.*, c.*');
        $this->db->where('a.id_user', $idPeserta);
        return $this->db->get('peserta a')->result_array();
    }
    
    // public function insert($data)
    // {
    //     return $this->db->insert('kelas', $data);
    // }

    public function showByIdKelas($key, $val)
    {
        $this->db->join('users b', 'b.id = a.id_user');
        return $this->db->get_where('peserta a', [$key => $val]);
    }

    public function daftarKelas($data)
    {
        return $this->db->insert('peserta', $data);
    }

    // public function update($id, $data)
    // {
    //     $this->db->where('id', $id);
    //     return $this->db->update('kelas', $data);
    // }

    // public function delete($id)
    // {
    //     $this->db->where('id', $id);
    //     return $this->db->delete('kelas');
    // }
}
