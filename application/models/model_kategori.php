<?php
    
class Model_kategori extends CI_Model {

    public function get()
    {
        return $this->db->get('kategori')->result_array();
    }

    public function show($key, $id)
    {
        return $this->db->get_where('kategori', [$key => $id]);
    }

    public function insert($data)
    {
        return $this->db->insert('kategori', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('kategori');
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('kategori', $data);
    }
}
