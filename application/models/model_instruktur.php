<?php
    
class Model_instruktur extends CI_Model {

    public function get()
    {
        return $this->db->get_where('users', ['role' => 'instruktur'])->result_array();
    }

    public function getSession()
    {
        return $this->db->query("SELECT * FROM  users where email='".$this->session->id."'")->row_array();
    }

    public function insert($data)
    {
        return $this->db->insert('users', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('users');
    }
}
