<?php
    
class Model_rating extends CI_Model {

    public function sum()
    {
        return $this->db->get('rating')->num_rows();
    }

    public function show($key, $id)
    {
        return $this->db->get_where('rating', [$key => $id]);
    }

    public function showID($key, $val)
    {
        $this->db->select('id_kelas');
        return $this->db->get_where('rating', [$key => $val])->result_array();
    }

    public function show2($key, $id, $key2, $id2)
    {
        return $this->db->get_where('rating', [$key => $id, $key2 => $id2]);
    }

    public function mean($idUser)
    {
        return $this->db->query("SELECT AVG(rating) as mean, id_user from rating where id_user = $idUser GROUP BY id_user")->row();
    }

    public function insert($data)
    {
        return $this->db->insert('rating', $data);
    }

    public function checkAssign()
    {
        $id_user    = $this->session->userdata('id_user');
        return $this->db->get_where('rating', ['id_user' => $id_user])->num_rows();
    }

    // public function delete($id)
    // {
    //     $this->db->where('id', $id);
    //     return $this->db->delete('kategori');
    // }

    // public function update($id, $data)
    // {
    //     $this->db->where('id', $id);
    //     return $this->db->update('kategori', $data);
    // }

    public function check($idUser, $idKelas)
    {
        return $this->db->get_where('rating', ['id_user' => $idUser, 'id_kelas' => $idKelas]);
    }
}
