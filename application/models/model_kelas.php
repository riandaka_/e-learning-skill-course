<?php
    
class Model_kelas extends CI_Model {

    public function get()
    {
        $this->db->join('users b', 'b.id = a.id_instruktur', 'left');
        $this->db->join('kategori c', 'c.id = a.id_kategori', 'left');
        $this->db->select('a.*, a.id as id_kelas, b.*, c.*');
        return $this->db->get('kelas a')->result_array();
    }

    public function getKelasByInstruktur()
    {
        $this->db->join('users b', 'b.id = a.id_instruktur', 'left');
        $this->db->join('kategori c', 'c.id = a.id_kategori', 'left');
        $this->db->select('a.*, a.id as id_kelas, b.*, c.*');
        $id_instruktur = $this->session->id_user;
        return $this->db->get_where('kelas a',['id_instruktur'=>$id_instruktur])->result_array();
    }
  
    public function count()
    {
        return $this->db->get('kelas');
    }
    
    public function insert($data)
    {
        return $this->db->insert('kelas', $data);
    }

    public function show($key, $val)
    {
        return $this->db->get_where('kelas', [$key => $val]);
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('kelas', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('kelas');
    }

    public function getID()
    {
        $this->db->select('id');
        return $this->db->get('kelas')->result_array();
    }
}
